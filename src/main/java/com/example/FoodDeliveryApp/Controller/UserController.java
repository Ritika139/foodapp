package com.example.FoodDeliveryApp.Controller;


import com.example.FoodDeliveryApp.Modal.User;
import com.example.FoodDeliveryApp.Repository.userRepository;
import com.example.FoodDeliveryApp.exception.ResourceNotFoundException;
import com.example.FoodDeliveryApp.services.cartService;
import com.example.FoodDeliveryApp.services.currentUserservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Optional;

@CrossOrigin()
@RestController
@RequestMapping("/users")
public class UserController {
    @Autowired
    private userRepository userRepository;

    @Autowired
    private cartService cartService;

    @Autowired
    private currentUserservice currentUserservice;
    @GetMapping("/get1")

    public String getallUser(){

        return "\"succesfully logedin\"";
    }

    @PostMapping("/adduser")
    public User createuser(@Valid @RequestBody User user) {
        user.setActive(1);
        user.setRole("user");
        return userRepository.save(user);
    }
    @PutMapping("/updateuser/{id}")
    public User updateUser(@PathVariable(value = "id") Long userid,
                            @Valid @RequestBody User userDetails) {

        User user = userRepository.findById(userid)
                .orElseThrow(() -> new ResourceNotFoundException("User", "user-id", userid));

        user.setUsername(userDetails.getUsername());
        user.setPassword(userDetails.getPassword());
        user.setEmail(userDetails.getEmail());
        user.setFirstname(userDetails.getFirstname());
        User updateduser = userRepository.save(user);
        return updateduser;
    }
    @GetMapping("/getuserdetails")
    public Optional<User> getuserdetails(Principal principal) {
        return cartService.getuserdetails(currentUserservice.getUserid(principal));
    }
}
