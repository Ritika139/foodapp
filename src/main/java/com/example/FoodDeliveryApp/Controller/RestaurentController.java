package com.example.FoodDeliveryApp.Controller;

import com.example.FoodDeliveryApp.Modal.restaurent;
import com.example.FoodDeliveryApp.Repository.resRepository;
import com.example.FoodDeliveryApp.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin()
@RestController
@RequestMapping("/api")
public class RestaurentController {
    private com.example.FoodDeliveryApp.Repository.resRepository resRepository;
    @Autowired
    public RestaurentController(resRepository resRepository)
    {
        this.resRepository = resRepository;
    }


    // Get All restaurents
    @GetMapping("/showrestaurents")
    public List<restaurent> getAllrestaurent() {
        return resRepository.findAll();
    }

    // Create a new
    @PostMapping("/addnewrestarent")
    public restaurent createitem(@Valid @RequestBody restaurent restaurent) {
        return resRepository.save(restaurent);
    }

    // Get a Single
    @GetMapping("/find-by-id/{id}")
    public restaurent getrestaurentById(@PathVariable(value = "id") Long resId) {
        return resRepository.findById(resId)
                .orElseThrow(() -> new ResourceNotFoundException("Product", "id", resId));
    }


    @GetMapping("/find-by-name/{name}")
    public List<restaurent> getrestaurentByName(@PathVariable(value = "name") String name) {

        return resRepository.findAllByResnameContaining(name);
    }

    @GetMapping("/find-by-location/{location}")
    public List<restaurent> getrestaurentByLocation(@PathVariable(value = "location") String location) {
        return resRepository.findAllByLocation(location);
    }

}
