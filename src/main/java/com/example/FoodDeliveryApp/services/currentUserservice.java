package com.example.FoodDeliveryApp.services;

import com.example.FoodDeliveryApp.Modal.User;
import com.example.FoodDeliveryApp.Repository.userRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Optional;


@Service
    public class currentUserservice {

        @Autowired
        com.example.FoodDeliveryApp.Repository.userRepository userRepository;

        public Long getUserid(Principal principal) {
            String email = principal.getName();
            Long id = userRepository.findByEmail(email).get().getUserid();
            return id;
        }
    }

