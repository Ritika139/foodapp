package com.example.FoodDeliveryApp.Repository;

import com.example.FoodDeliveryApp.Modal.items;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface itemRepository  extends JpaRepository<items, Long> {
}
