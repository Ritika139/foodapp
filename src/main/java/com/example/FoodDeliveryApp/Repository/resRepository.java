package com.example.FoodDeliveryApp.Repository;

import com.example.FoodDeliveryApp.Modal.restaurent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface resRepository extends JpaRepository<restaurent,Long> {
    @Override
    List<restaurent> findAll();

    List<restaurent> findAllByResnameContaining(String name);
    List<restaurent> findAllByLocation(String Category);
}
