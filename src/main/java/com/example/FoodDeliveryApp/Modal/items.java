package com.example.FoodDeliveryApp.Modal;

import javax.persistence.*;

@Entity
public class items {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long itemId;
    private String type;
    private long price;
    private String content;
    @OneToOne
    private restaurent resId;

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long itemId) {
        this.itemId = itemId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public restaurent getResId() {
        return resId;
    }

    public void setResId(restaurent resId) {
        this.resId = resId;
    }
}
